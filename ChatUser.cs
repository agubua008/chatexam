﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatExam
{
    public class ChatUser
    {
        public string Name { get; private set; }
        private string _lastMessage;
        private DateTime? _connectionDate;
        private DateTime? _lastMessageDate;

        public string LastMessage
        {
            get
            {
                if (_lastMessageDate.HasValue)
                    return $"{_lastMessageDate.Value:HH:mm:ss} {Name} => {_lastMessage}";
                return $"XX:XX:XX => {_lastMessage}";
            }
        }

        public ChatUser(string name)
        {
            this._connectionDate = DateTime.Now;
            this.Name = name;
        }

        public void AddNewMessage(string message)
        {
            _lastMessage = message;
            _lastMessageDate = DateTime.Now;
        }
    }
}
