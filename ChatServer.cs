﻿using Fleck;
using log4net;
using System;

namespace ChatExam
{
    public class ChatServer : AbstractChatWebSocket
    {
        private IWebSocketConnection _socket;
        private WebSocketServer _server = null;
        public ChatServer()
        {
            logger = LogManager.GetLogger(typeof(ChatServer));
        }

        public override bool Start(int port, string name)
        {
            try
            {
                _server = new WebSocketServer($"ws://127.0.0.1:{port}");
                _server.RestartAfterListenError = true;
                _server.ListenerSocket.NoDelay = true;
                _server.Start(socket =>
                {
                    _socket = socket;
                    _socket.OnOpen += onOpenConnection;
                    _socket.OnClose += onCloseConnection;
                    _socket.OnMessage += onMessage;
                    _socket.OnError += onError;
                });
                logger.Info($"Chat Server Start OK on port: {port}");

                localUser = new ChatUser(name);

                clearConsoleAndShowExitMethod();
                showWaitingMessage();

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Server Start Error", ex);
                if (_server != null)
                {
                    _server.Dispose();
                    _server = null;
                }
                return false;
            }
        }

        public override void Stop()
        {
            if (_server != null)
                _server.Dispose();
        }

        public override bool SendMessage(string message)
        {
            try
            {
                if (_server == null || _socket == null)
                    return false;
                var jsonMsg = new ChatMessage(message).JsonChatMessage;
                _socket.Send(jsonMsg);
                localUser.AddNewMessage(message);
                Console.WriteLine(localUser.LastMessage);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Server SendMessage Error", ex);
                return false;
            }
        }


        private bool sendLogin()
        {
            try
            {
                if (_server == null || _socket == null)
                    return false;
                var jsonMsg = new ChatMessage(localUser.Name, ChatMessageType.Login).JsonChatMessage;
                _socket.Send(jsonMsg);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Server SendMessage Error", ex);
                return false;
            }
        }

        private void onOpenConnection()
        {
            clearConsoleAndShowExitMethod();

            logger.Info("A user is connected to the chat");
            stateConnection = true;
            remoteUser = null;

            sendLogin();
        }

        private void onCloseConnection()
        {
            clearConsoleAndShowExitMethod();
            showClientLeaveToTheChat();
            showWaitingMessage();

            logger.Info($"The user {remoteUser?.Name} is leave the chat");
            stateConnection = false;
            remoteUser = null;
        }

        private void onMessage(string jsonMessage)
        {
            base.onMessageInternal(jsonMessage);
        }

        private void onError(Exception error)
        {
            logger.Error($"An error occurred {error?.Message}", error);
        }


    }
}
