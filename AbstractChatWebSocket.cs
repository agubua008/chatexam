﻿using log4net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;

namespace ChatExam
{
    public abstract class AbstractChatWebSocket
    {
        protected ILog logger;
        protected ChatUser remoteUser = null;
        protected ChatUser localUser = null;
        protected bool stateConnection = false;

        public abstract bool Start(int port, string name);
        public abstract void Stop();
        public abstract bool SendMessage(string name);

        protected void onMessageInternal(string jsonMessage)
        {
            try
            {
                var jObjectMessage = JObject.Parse(jsonMessage);
                if (!jObjectMessage.IsValid(ChatMessage.GetJSchema))
                {
                    logger.Error($"An error occurred, the message format is invalid, JsonMessage: {jsonMessage}");
                    return;
                }
                var chatMessage = jObjectMessage.ToObject<ChatMessage>();
                if (chatMessage.Type == ChatMessageType.Login)
                {
                    remoteUser = new ChatUser(chatMessage.Message);
                    logger.Info($"The user {remoteUser?.Name} is connected to the chat");
                    showClientEnterToTheChat();
                }
                else if (chatMessage.Type == ChatMessageType.Message)
                {
                    remoteUser?.AddNewMessage(chatMessage.Message);
                    logger.Info($"The user {remoteUser?.Name} send '{chatMessage.Message}' to you");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine(remoteUser.LastMessage);
                    Console.ResetColor();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"An exception occurred onMessage, error: {ex.Message}, JsonMessage: {jsonMessage}", ex);
            }
        }

        protected void showClientEnterToTheChat()
        {
            Console.WriteLine($"El usuario {remoteUser?.Name} ingreso al chat");
        }

        protected void showClientLeaveToTheChat()
        {
            Console.WriteLine($"El usuario {remoteUser?.Name} dejo el chat");
        }

        protected void clearConsoleAndShowExitMethod()
        {
            Console.Clear();
            Console.WriteLine("Precione CTRL + Z para salir");
        }

        protected void showWaitingMessage()
        {
            Console.WriteLine("Esperando a que un usuario se conecte al chat...");
        }

        protected void showCloseMessage()
        {
            Console.WriteLine("Debe cerrar la ventana para inciar una nueva sesión");
        }
    }
}
