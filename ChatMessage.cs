﻿using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;

namespace ChatExam
{
    public class ChatMessage
    {
        public ChatMessageType Type { get; set; }
        public string Message { get; set; }

        public ChatMessage(string message, ChatMessageType type = ChatMessageType.Message)
        {
            this.Message = message;
            this.Type = type;
        }

        [JsonIgnore]
        public string JsonChatMessage
        {
            get
            {
                return JsonConvert.SerializeObject(this);
            }
        }

        [JsonIgnore]
        public static JSchema GetJSchema
        {
            get
            {
                if (_internalJSchema != null) return _internalJSchema;
                _internalJSchema = new JSchemaGenerator().Generate(typeof(ChatMessage));
                return _internalJSchema;
            }
        }
        [JsonIgnore]
        private static JSchema _internalJSchema;
    }
}
