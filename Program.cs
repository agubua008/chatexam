﻿using Fleck;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;
using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using WebSocketSharp;

namespace ChatExam
{
    class Program
    {
        static void Main(string[] args)
        {
            var port = 8889;
            if (args != null && args.Length > 0)
                int.TryParse(args[0], out port);

            Console.WriteLine("Bienvenido al sistema de chat local");
            Console.WriteLine($"El puerto es: {port}");
            Console.WriteLine("Ingrese su nombre y precione enter para continuar:");
            var name = Console.ReadLine();

            AbstractChatWebSocket chat;
            if (portIsAvailable(port))
                chat = new ChatServer();
            else
                chat = new ChatClient();
            chat.Start(port, name);

            StringBuilder buffer = new StringBuilder();
            ConsoleKeyInfo cki;
            do
            {
                cki = Console.ReadKey();
                if (cki.Key == ConsoleKey.Enter)
                {
                    chat.SendMessage(buffer.ToString());
                    buffer.Clear();
                }
                else buffer.Append(cki.KeyChar);
            }
            while (!(cki.Modifiers == ConsoleModifiers.Control && cki.Key == ConsoleKey.Z));
            buffer.Clear();
            chat.Stop();
            Console.Clear();
        }


        private static bool portIsAvailable(int port)
        {
            var isAvailable = !IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners().Any(n => n.Port == port);
            return isAvailable;
        }
    }
}
