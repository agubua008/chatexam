﻿using log4net;
using System;
using WebSocketSharp;

namespace ChatExam
{
    public class ChatClient : AbstractChatWebSocket
    {
        private WebSocket _client = null;
        public ChatClient()
        {
            logger = LogManager.GetLogger(typeof(ChatClient));
        }

        public override bool Start(int port, string name)
        {
            try
            {
                _client = new WebSocket($"ws://127.0.0.1:{port}");

                _client.OnOpen += onOpenConnection;
                _client.OnClose += onCloseConnection;
                _client.OnMessage += onMessage;
                _client.OnError += onError;

                _client.Connect();

                localUser = new ChatUser(name);

                sendLogin();

                logger.Info($"Chat Client Start OK");
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Client Start Error", ex);
                if (_client != null)
                {
                    _client.Close();
                    _client = null;
                }
                return false;
            }
        }

        public override void Stop()
        {
            if (_client != null)
                _client.Close();
        }

        public override bool SendMessage(string message)
        {
            try
            {
                if (_client == null)
                    return false;
                var jsonMsg = new ChatMessage(message).JsonChatMessage;
                _client.Send(jsonMsg);
                localUser.AddNewMessage(message);
                Console.WriteLine(localUser.LastMessage);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Client SendMessage Error", ex);
                return false;
            }
        }


        private bool sendLogin()
        {
            try
            {
                if (_client == null)
                    return false;
                var jsonMsg = new ChatMessage(localUser.Name, ChatMessageType.Login).JsonChatMessage;
                _client.Send(jsonMsg);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Chat Server SendMessage Error", ex);
                return false;
            }
        }

        private void onOpenConnection(object sender, EventArgs e)
        {
            clearConsoleAndShowExitMethod();
            logger.Info("A user is connected to the chat");
            stateConnection = true;
            remoteUser = null;

        }

        private void onCloseConnection(object sender, CloseEventArgs e)
        {
            logger.Info($"The user {remoteUser?.Name} is leave the chat");
            stateConnection = false;
            remoteUser = null;
            Environment.Exit(0);
        }

        private void onMessage(object sender, MessageEventArgs e)
        {
            var jsonMessage = e?.Data;
            base.onMessageInternal(jsonMessage);
        }

        private void onError(object sender, ErrorEventArgs e)
        {
            logger.Error($"An error occurred {e.Message}", e.Exception);
        }

    }
}
