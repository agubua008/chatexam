# ChatExam

Proyecto Chat Local desarrollado en C# para VOICEMOD


Para compilar el proyecto se utilizo Visual Studio 2019 y .NetFramework 4.7.2, ya que no existia ninguna restriccion de versiones de Framework.


Para iniciar el chat se debej ejecutar el archivo **ChatExam.exe** que opcionalmente puede recibir como primer parametro el puerto, por defecto el chat usa el puerto 8889.

Como el PDF lo indica el chat es accesible de forma local, solo se hace bind sobre 127.0.0.1, por lo cual no es accesible desde dos PCs en la red.

**Ejemplo puerto por defecto**

`ChatExam.exe`

**Ejemplo puerto informado**

`ChatExam.exe 9555`

*  Una ves que programa inicio le solicitara el su nombre, debe ingresarlo y luego precionar Enter.
*  Para salir del programa se debe usar las teclas CTRL + Z.
*  El primer programa que se inicia auspicia de servidor y el segundo de cliente.
*  Si un cliente se desconecta el server queda esperando la conexion de un nuevo cliente.
*  Si el server se desconecta, al cliente automaticamente se cierra y se debe volver a iniciar el chat para volver a conectarlo.
*  Para el transporte de los mensajes mediante WebSocket se uso un objeto Json Serializado para indicar de que tipo de mensaje se trata.
*  Existen dos tipos de mensajes Login (inicio de sesion) o Message (mensaje propiamente dicho).
*  El programa logea lo que sucede a traves de la librería log4net, existe un archivo de configuracion que indica donde y con el nivel se realiza el log.
*  Los mensajes enviados de los recibidos en la consola se diferencian mediante un color.
*  Para incluir todas las librerias en el exe, se uso la libreria de Costura.

